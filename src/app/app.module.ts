import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule, CanActivate } from '@angular/router';

import { MaterialModule } from './material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';

//firebase settings
import { environment } from '../environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database'
import { AngularFireAuthModule } from '@angular/fire/auth';
import { UserRegisterComponent } from './user-register/user-register.component';


import { LoginService } from './services/login.service';
import { FrontcoverComponent } from './frontcover/frontcover.component';
import { HeaderComponent } from './header/header.component';
import { PeliculasComponent } from './peliculas/peliculas.component';

import {AuthGuard } from "./guards/auth.guard";
import { AdminPageComponent } from './admin-page/admin-page.component';





const appRoutes: Routes = [

  { path: '', component: FrontcoverComponent },
  { path: 'home', component: HomeComponent, },
  { path: 'login', component: LoginComponent},
  { path: 'register', component: UserRegisterComponent },
  { path: 'peliculas', component: PeliculasComponent},
  


];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    UserRegisterComponent,
    FrontcoverComponent,
    HeaderComponent,
    PeliculasComponent,
    AdminPageComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,

    //firebase setting imports
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule

  ],
  providers: [
    LoginService,
    AuthGuard,

  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
