import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { LoginService } from '../services/login.service';
import { Router } from '@angular/router';
import { auth } from 'firebase';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public isLogin: Boolean;
  public nameuser: string;
  public emailuser: string;

  constructor(private loginService: LoginService, private router: Router, public angularfireauth: AngularFireAuth) {



  }

  ngOnInit() {
    this.loginService.getAuth().subscribe(auth =>{
      if(auth){
        this.isLogin = true;
        this.nameuser = auth.displayName;
        this.emailuser = auth.email;
      } else {
        this.isLogin = false;
      }
    })
  }

  public onclickLogout(){

    this.loginService.signOut();

  }
}
