import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router';

import { PeliculasService } from '../services/peliculas.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  status: any = 'create';
    peliculas = null;
    pelicula: any = {};
    textButton: string = null;

    constructor(private fireBaseService:  PeliculasService, private route: ActivatedRoute) {
        this.peliculas = fireBaseService.getPelicula();
        // this.status = this.route.snapshot.params['status'];
        console.log(this.status);
        this.textButton = (this.status == 'create') ? 'Crear post' : 'Actualizar';
    }

    ngOnInit() {
    }

    validateStatusButton(){
        if (this.status == 'create') {
            this.createVehicle();
        } else {
            this.updateVehicle();
        }
    }


    getVehicle(id) {
        this.fireBaseService.getpelicula(id).valueChanges().subscribe(vehicle => {
            this.pelicula = vehicle;
            this.textButton = 'Actualizar';
            this.status = id;
        });
    }

    cancelVehicle() {
        this.status = 'create';
        this.textButton = 'Crear Pelicula';
        this.pelicula = {};
    }

    createVehicle() {
        this.pelicula.id = Date.now();
        this.fireBaseService.createPelicula(this.pelicula);
        this.cancelVehicle();
    }

    updateVehicle() {
        this.fireBaseService.updatePelicula(this.pelicula);
        this.cancelVehicle();
    }

    deleteVehicle(id) {
        this.fireBaseService.deletePelicula(id);
    }

    
}
