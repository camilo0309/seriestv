import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireStorage } from '@angular/fire/storage';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class PeliculasService {
    constructor(private db: AngularFireDatabase) { }

    // Metodo para listar todas los vehiculos
    public getPelicula() {
      return this.db.list('pelicula').valueChanges();
    }
  
    // Metodo para obtener un solo vehiculo
    public getpelicula(id) {
      return this.db.object('pelicula/' + id);
    }
  
    // Metodo crear un nuevo vehiculo
    public createPelicula(peliculas) {
      this.db.database.ref('pelicula/' + peliculas.id).set(peliculas);
    }
  
    // Metodo para actualizar los datos de un vehiculo
    public updatePelicula(peliculas) {
      this.db.database.ref('pelicula/' + peliculas.id).set(peliculas);
    }
  
    // Metodo para eliminar un vehiculo
    public deletePelicula(id) {
      this.db.object('pelicula/' + id).remove();
    }
  }
  