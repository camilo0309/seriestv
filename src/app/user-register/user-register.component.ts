import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css']
})
export class UserRegisterComponent implements OnInit {


  login: any = {};

  isAuthenticated: boolean = true;

  constructor(private authentication: LoginService, private router: Router, private route: ActivatedRoute) {
    authentication.isAuthenticated().subscribe((result) => {
      if (result && result.uid) {
        this.isAuthenticated = true;
        this.router.navigate(['home']);
      } else {
        this.isAuthenticated = false;
       
      }
    }, (error) => {
      this.isAuthenticated = false;
    });
  }

  ngOnInit() {

  }

  public register() {
    
    this.authentication.register(this.login.email, this.login.password, this.login);
  }
}
