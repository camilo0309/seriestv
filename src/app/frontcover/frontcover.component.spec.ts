import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontcoverComponent } from './frontcover.component';

describe('FrontcoverComponent', () => {
  let component: FrontcoverComponent;
  let fixture: ComponentFixture<FrontcoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrontcoverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontcoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
